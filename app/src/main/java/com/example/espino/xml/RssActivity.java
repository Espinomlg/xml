package com.example.espino.xml;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;

import com.example.espino.xml.R;
import com.example.espino.xml.adapters.RSSAdapter;
import com.example.espino.xml.models.RSSNews;

/**
 * Created by espino on 11/12/16.
 */

public class RssActivity extends ListActivity {

    private static final int NEWS = 2;

    private RSSAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        adapter = new RSSAdapter(getApplicationContext());

        getListView().setAdapter(adapter);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RSSNews news = adapter.getItem(position);
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                i.putExtra("action", NEWS);
                i.putExtra("news", news);
                startActivity(i);
            }
        });

    }
}
