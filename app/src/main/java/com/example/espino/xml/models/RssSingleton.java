package com.example.espino.xml.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by espino on 11/12/16.
 */

public class RssSingleton {

    private static RssSingleton instance;
    private static ArrayList<RSSNews> list;

    private RssSingleton(){
        list = new ArrayList<>();
        list.add(new RSSNews("El País", "http://ep00.epimg.net/rss/elpais/portada.xml"));
        list.add(new RSSNews("El Mundo", "http://estaticos.elmundo.es/elmundo/rss/espana.xml"));
    }

    public static RssSingleton getInstance(){

        if(instance == null)
            instance = new RssSingleton();

        return instance;
    }

    public ArrayList<RSSNews> getList(){
        return list;
    }
}
