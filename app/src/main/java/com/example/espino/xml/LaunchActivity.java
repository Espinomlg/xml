package com.example.espino.xml;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LaunchActivity extends AppCompatActivity {

    private static final int METEO = 0,
    BIKES = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
    }

    public void launch(View v){
        Intent i = null;
        switch (v.getId()){
            case R.id.laucher_btn1:
                startActivity(new Intent(LaunchActivity.this,EnterpriseActivity.class));
                break;
            case R.id.launcher_btn2:
                i = new Intent(LaunchActivity.this, SplashScreenActivity.class);
                i.putExtra("action", METEO);
                startActivity(i);
                break;
            case R.id.launcher_btn3:
                i = new Intent(LaunchActivity.this, SplashScreenActivity.class);
                i.putExtra("action", BIKES);
                startActivity(i);
                break;
            case R.id.launcher_btn4:
                i = new Intent(LaunchActivity.this, RssActivity.class);
                startActivity(i);
                break;
        }
    }
}
