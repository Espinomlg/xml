package com.example.espino.xml.models;

/**
 * Created by espino on 7/12/16.
 */

public class Employee {

    private String name;
    private String job;
    private int age;
    private float salary;

    public Employee(String name, String job, int age, float salary) {
        this.name = name;
        this.job = job;
        this.age = age;
        this.salary = salary;
    }

    public Employee() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee:\n" +
                "\tname:" + name + '\n' +
                "\tjob:" + job + '\n' +
                "\tage:" + age + '\n' +
                "\tsalary:" + salary;
    }
}
