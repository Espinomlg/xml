package com.example.espino.xml.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.espino.xml.R;
import com.example.espino.xml.models.BikeStation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by espino on 10/12/16.
 */
public class BikesAdapter extends ArrayAdapter<BikeStation>{



    public BikesAdapter(Context context, ArrayList<BikeStation> list) {
        super(context, R.layout.listitem_station, list);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View item = convertView;
        BikeStationHolder holder;

        if(item == null){
            item = LayoutInflater.from(getContext()).inflate(R.layout.listitem_station, parent, false);
            holder = new BikeStationHolder();

            holder.address = (TextView) item.findViewById(R.id.listitem_txv);

            item.setTag(holder);
        }
        else
            holder = (BikeStationHolder) item.getTag();

        holder.address.setText(getItem(position).getAddress());

        return item;
    }

    private static class BikeStationHolder{
        private TextView address;
    }
}
