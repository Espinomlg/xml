package com.example.espino.xml;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.espino.xml.models.Meteorology;
import com.example.espino.xml.models.RSSNews;
import com.example.espino.xml.models.RestClient;
import com.example.espino.xml.xmlMethods.XMLMethods;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by espino on 10/12/16.
 */

public class SplashScreenActivity extends AppCompatActivity {

    private static final String AEMET = "http://www.aemet.es/xml/municipios/localidad_29067.xml";
    private static final String BIKES = "http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/estacion-bicicleta.xml";
    private Meteorology[] meteo;
    private ArrayList list;
    private RSSNews news;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        int action = getIntent().getIntExtra("action", -1);
        switch (action){
            case 0:
                download(AEMET);
                break;
            case 1:
                download(BIKES);
                break;
            case 2:
                news = (RSSNews) getIntent().getParcelableExtra("news");
                download(news.getLink());
        }

    }



    private void download(String url) {
        final String web = url;
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "aemet.xml");

        RestClient.get(url, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Descarga OK\n " + file.getPath(), Toast.LENGTH_LONG).show();
                try {
                    launch(web, file);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void launch(String web, File file) throws IOException, XmlPullParserException {
        Intent i = null;

        switch (web){
            case AEMET:
                meteo = XMLMethods.analyzeAemet(file);
                i = new Intent(getApplicationContext(), MeteorologyActivity.class);
                i.putExtra("meteo", meteo);
                startActivity(i);
                finish();
                break;

            case BIKES:
                list = XMLMethods.analyzeStations(file);
                i = new Intent(getApplicationContext(), BikeStationsActivity.class);
                i.putExtra("stations", list);
                startActivity(i);
                finish();
                break;

            default:
                list = XMLMethods.analyzeRss(file);
                i = new Intent(getApplicationContext(), NewsActivity.class);
                i.putExtra("news", list);
                startActivity(i);
                finish();
                break;

        }
    }
}
