package com.example.espino.xml;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.espino.xml.models.Employee;
import com.example.espino.xml.xmlMethods.XMLMethods;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by espino on 7/12/16.
 */

public class EnterpriseActivity extends AppCompatActivity {

    private TextView averageAge,
    minSalary,
    maxSalary;
    private LinearLayout linear;

    private ArrayList<Employee> enterprise;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise);

        averageAge = (TextView) findViewById(R.id.entrerprise_txv_averageage);
        minSalary = (TextView) findViewById(R.id.enterprise_txv_minsalary);
        maxSalary = (TextView) findViewById(R.id.enterprise_txv_maxsalary);
        linear = (LinearLayout) findViewById(R.id.enterprise_linearlayout);

        try {
            enterprise = XMLMethods.analyzeFileResource(getApplicationContext());
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Ha habido un problema de E/S", Toast.LENGTH_SHORT).show();
        } catch (XmlPullParserException e) {
            Toast.makeText(getApplicationContext(), "Ha habido un problema con el archivo xml", Toast.LENGTH_SHORT).show();
        }

        mostrarEmpleados();

    }

    public void calculate(View v){
        float age = 0;
        float min = Integer.MAX_VALUE,
                max = Integer.MIN_VALUE;

        for(Employee emp : enterprise){
            age += emp.getAge();
            if(emp.getSalary() < min)
                min = emp.getSalary();
            if(emp.getSalary() > max)
                max = emp.getSalary();
        }

        age = age / enterprise.size();
        DecimalFormat decfor = new DecimalFormat("0.00");
        averageAge.setText(String.format(getResources().getString(R.string.formated_edad_media), decfor.format(age)));
        minSalary.setText(String.format(getResources().getString(R.string.formated_salario_maximo), decfor.format(max)));
        maxSalary.setText(String.format(getResources().getString(R.string.formated_salario_minimo), decfor.format(min)));
    }

    public void mostrarEmpleados(){
        for(Employee emp: enterprise){
            TextView txv = new TextView(getApplicationContext());
            txv.setText(emp.toString());
            txv.setTextColor(getResources().getColor(R.color.black));

            linear.addView(txv);
        }
    }
}
