package com.example.espino.xml.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.espino.xml.R;
import com.example.espino.xml.models.RSSNews;
import com.example.espino.xml.models.RssSingleton;

/**
 * Created by espino on 11/12/16.
 */

public class RSSAdapter extends ArrayAdapter<RSSNews> {



    public RSSAdapter(Context context) {
        super(context, R.layout.listitem_station, RssSingleton.getInstance().getList());
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View item = convertView;
        RSSAdapter.NewsHolder holder;

        if(item == null){
            item = LayoutInflater.from(getContext()).inflate(R.layout.listitem_station, parent, false);
            holder = new RSSAdapter.NewsHolder();

            holder.name = (TextView) item.findViewById(R.id.listitem_txv);

            item.setTag(holder);
        }
        else
            holder = (RSSAdapter.NewsHolder) item.getTag();

        holder.name.setText(getItem(position).getName());

        return item;
    }

    private static class NewsHolder{
        private TextView name;
    }
}

