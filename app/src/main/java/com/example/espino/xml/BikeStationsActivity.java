package com.example.espino.xml;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.espino.xml.adapters.BikesAdapter;
import com.example.espino.xml.models.BikeStation;
import com.example.espino.xml.models.RestClient;
import com.example.espino.xml.xmlMethods.XMLMethods;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by espino on 10/12/16.
 */

public class BikeStationsActivity extends ListActivity {

    private static final String WEB = "http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/estacion-bicicleta.xml";
    private ArrayList<BikeStation> list;
    private BikesAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bikestations);

        list = (ArrayList<BikeStation>) getIntent().getSerializableExtra("stations");
        //download(WEB);
        adapter = new BikesAdapter(getApplicationContext(), list);
        getListView().setAdapter(adapter);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BikeStation station = adapter.getItem(position);
                Intent i = new Intent(getApplicationContext(), StationDataActivity.class);
                i.putExtra("station", station);
                startActivity(i);
            }
        });
    }

    public void load(View v){

        adapter = new BikesAdapter(getApplicationContext(), list);
        getListView().setAdapter(adapter);
    }

    private void download(String url) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "estaciones.xml");

        RestClient.get(url, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Descarga OK\n " + file.getPath(), Toast.LENGTH_LONG).show();
                try {
                    list = XMLMethods.analyzeStations(file);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }

            }
        });

    }
}
