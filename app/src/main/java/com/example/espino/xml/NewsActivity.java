package com.example.espino.xml;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.example.espino.xml.adapters.NewsAdapter;
import com.example.espino.xml.models.New;

import java.util.ArrayList;


public class NewsActivity extends ListActivity{

    private NewsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        adapter = new NewsAdapter(getApplicationContext(), (ArrayList<New>) getIntent().getSerializableExtra("news"));
        getListView().setAdapter(adapter);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(adapter.getItem(position).getLink()));
                startActivity(i);
            }
        });
    }
}
