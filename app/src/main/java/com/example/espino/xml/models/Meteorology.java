package com.example.espino.xml.models;

import java.io.Serializable;


public class Meteorology implements Serializable{
    private String[] rainProb,
    snow,
    wind,
    sky;
    private String temp,
    uv;

    public Meteorology(){
        rainProb = new String[3];
        snow = new String[3];
        wind = new String[3];
        sky = new String[3];
        temp = "";
        uv = "";
    }

    public String getRainProb(int index) {
        return rainProb[index];
    }

    public void setRainProb(int index, String value) {
        this.rainProb[index] = value == null ? "0" : value;
    }

    public String getSnow(int index) {
        return snow[index];
    }

    public void setSnow(int index, String value) {
        this.snow[index] = value == null ? "0" : value;
    }


    public String getWind(int index) {
        return wind[index];
    }

    public void setWind(int index, String value) {
        this.wind[index] = value == null ? "0" : value;
    }


    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp += temp;
    }

    public String getUv() {
        return uv;
    }

    public void setUv(String uv) {
        this.uv = uv;
    }

    public String getSky(int index) {
        return sky[index];
    }

    public void setSky(int index, String value) {
        this.sky[index] = value == null ? "0" : value;
    }
}
