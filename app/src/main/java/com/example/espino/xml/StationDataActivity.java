package com.example.espino.xml;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.TextView;

import com.example.espino.xml.models.BikeStation;

/**
 * Created by espino on 10/12/16.
 */
public class StationDataActivity extends AppCompatActivity{

    private TextView state, available, anchorages, lastmod, title;
    private WebView web;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stationdata);

        title = (TextView) findViewById(R.id.stationdata_txv_title);
        state = (TextView) findViewById(R.id.stationdata_txv_state);
        available = (TextView) findViewById(R.id.stationdata_txv_available);
        anchorages = (TextView) findViewById(R.id.stationdata_txv_anchorages);
        lastmod = (TextView) findViewById(R.id.stationdata_txv_lastmodification);
        web = (WebView) findViewById(R.id.stattiondata_webview);

        loaddata();
    }

    public void loaddata(){
        BikeStation bs = getIntent().getParcelableExtra("station");

        title.setText(bs.getAddress());
        state.setText(String.format(getResources().getString(R.string.stationdata_state), bs.getState()));
        available.setText(String.format(getResources().getString(R.string.stationdata_available), bs.getBikesAvailable()));
        anchorages.setText(String.format(getResources().getString(R.string.stationdata_anchorages), bs.getAnchorages()));
        lastmod.setText(String.format(getResources().getString(R.string.stationdata_lastmod), bs.getLastModification()));
        web.loadUrl(bs.getLink());
    }
}
