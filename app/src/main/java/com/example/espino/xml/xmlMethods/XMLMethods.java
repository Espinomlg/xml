package com.example.espino.xml.xmlMethods;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Xml;

import com.example.espino.xml.R;
import com.example.espino.xml.models.BikeStation;
import com.example.espino.xml.models.Employee;
import com.example.espino.xml.models.Meteorology;
import com.example.espino.xml.models.New;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by espino on 7/12/16.
 */

public class XMLMethods {

    public static ArrayList<Employee> analyzeFileResource(Context c) throws IOException, XmlPullParserException {

        boolean employee = false;
        ArrayList<Employee> enterprise = new ArrayList<>();
        XmlResourceParser xrp = c.getResources().getXml(R.xml.enterprise);

        int eventType = xrp.next();
        Employee emp = new Employee();
        while (eventType != XmlPullParser.END_DOCUMENT) {

            switch (eventType){
                case XmlPullParser.START_TAG:
                    if(xrp.getName().equals("employee"))
                        employee = true;
                    else if(employee && xrp.getName().equals("name"))
                        emp.setName(xrp.nextText());
                    else if(employee && xrp.getName().equals("job"))
                        emp.setJob(xrp.nextText());
                    else if(employee && xrp.getName().equals("age"))
                        emp.setAge(Integer.valueOf(xrp.nextText()));
                    else if(employee && xrp.getName().equals("salary"))
                        emp.setSalary(Float.valueOf(xrp.nextText()));
                    break;

                case XmlPullParser.END_TAG:
                    if(xrp.getName().equals("employee")) {
                        enterprise.add(emp);
                        employee = false;
                        emp = new Employee();
                    }
                    break;
            }
            eventType = xrp.next();
        }

        return enterprise;
    }

    public static ArrayList<New> analyzeRss(File xml) throws IOException, XmlPullParserException{
        ArrayList<New> list = new ArrayList<>();
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(xml));
        boolean item = false;
        New n = new New();
        int eventType = xpp.next();

        while(eventType != XmlPullParser.END_DOCUMENT){
            switch (eventType){
                case XmlPullParser.START_TAG:
                    if(xpp.getName().equals("item"))
                        item = true;
                    else if(item && xpp.getName().equals("title"))
                        n.setTitle(xpp.nextText().replaceAll("\\w&&[CDAT]",""));
                    else if(item && xpp.getAttributeCount() == 0 && xpp.getName().equals("link")) {
                        n.setLink(replaceString(xpp.nextText()));
                        item = false;
                    }
                    break;

                case XmlPullParser.END_TAG:
                    if(xpp.getName().equals("item")){
                        list.add(n);
                        n = new New();
                    }
                    break;
            }
            eventType = xpp.next();
        }

        return list;
    }


    public static ArrayList<BikeStation> analyzeStations(File xml) throws IOException, XmlPullParserException{
        ArrayList<BikeStation> list = new ArrayList<>();
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(xml));
        boolean station = false;
        BikeStation bs = new BikeStation();
        int eventType = xpp.next();

        while(eventType != XmlPullParser.END_DOCUMENT){
            switch (eventType){
                case XmlPullParser.START_TAG:
                    if(xpp.getName().equals("estacion"))
                        station = true;
                    else if(station && xpp.getName().equals("uri"))
                        bs.setLink(xpp.nextText());
                    else if(station && xpp.getName().equals("title"))
                        bs.setAddress(xpp.nextText());
                    else if(station && xpp.getName().equals("estado"))
                        bs.setState(xpp.nextText());
                    else if(station && xpp.getName().equals("bicisDisponibles"))
                        bs.setBikesAvailable(xpp.nextText());
                    else if(station && xpp.getName().equals("anclajesDisponibles"))
                        bs.setAnchorages(xpp.nextText());
                    else if(station && xpp.getName().equals("lastUpdated"))
                        bs.setLastModification(xpp.nextText());
                    break;

                case XmlPullParser.END_TAG:
                    if(xpp.getName().equals("estacion")){
                        station = false;
                        list.add(bs);
                        bs = new BikeStation();
                    }
                    break;
            }
            eventType = xpp.next();
        }

        return list;
    }

    public static Meteorology[] analyzeAemet(File xml) throws IOException, XmlPullParserException {

        Meteorology[] meteo = new Meteorology[2];
        meteo[0] = new Meteorology();
        meteo[1] = new Meteorology();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = GregorianCalendar.getInstance();
        String today = formater.format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH,1);
        String tomorrow = formater.format(cal.getTime());
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(xml));
        boolean day = false;
        boolean wind = false;
        boolean temp = false;
        byte index = -1;
        String windPeriod = "";
        int eventType = xpp.next();

        while(eventType != XmlPullParser.END_DOCUMENT){
            switch (eventType){
                case XmlPullParser.START_TAG:
                    if(xpp.getName().equals("dia")){
                        if(xpp.getAttributeValue(0).equals(today)){
                            index = 0;
                            day = true;
                        }
                        else if(xpp.getAttributeValue(0).equals(tomorrow)){
                            index = 1;
                            day = true;
                        }
                    }

                    else if(day && xpp.getName().equals("prob_precipitacion"))
                        setValueForPeriod(meteo[index], xpp.getName(), xpp.getAttributeValue(0),xpp.nextText());
                    else if(day && xpp.getName().equals("cota_nieve_prov"))
                        setValueForPeriod(meteo[index], xpp.getName(), xpp.getAttributeValue(0),xpp.nextText());
                    else if(day && xpp.getName().equals("estado_cielo"))
                        setValueForPeriod(meteo[index], xpp.getName(), xpp.getAttributeValue(0),xpp.nextText());
                    else if(day && xpp.getName().equals("viento")){
                        wind = true;
                        windPeriod = xpp.getAttributeValue(0);
                    }
                    else if(day && wind && xpp.getName().equals("velocidad"))
                        setValueForPeriod(meteo[index], "viento", windPeriod, xpp.nextText());
                    else if(day && xpp.getName().equals("temperatura"))
                        temp = true;
                    else if(day && temp && xpp.getName().equals("maxima"))
                        meteo[index].setTemp(xpp.nextText());
                    else if(day && temp && xpp.getName().equals("minima"))
                        meteo[index].setTemp("/" + xpp.nextText());
                    else if(day && xpp.getName().equals("uv_max"))
                        meteo[index].setUv(xpp.nextText());
                    break;

                case XmlPullParser.END_TAG:
                    if(xpp.getName().equals("dia"))
                        day = false;
                    else if(xpp.getName().equals("viento"))
                        wind = false;
                    else if(xpp.getName().equals("temperatura"))
                        temp = false;
                    break;
            }
            eventType = xpp.next();
        }

        return meteo;
    }

    private static void setValueForPeriod(Meteorology meteo, String eventName, String period, String value) {
        if (eventName.equals("prob_precipitacion")) {
            switch (period) {
                case "06-12":
                    meteo.setRainProb(0, value + "/");
                    break;
                case "12-18":
                    meteo.setRainProb(1, value + "/");
                    break;
                case "18-24":
                    meteo.setRainProb(2, value);
                    break;
            }
        } else if (eventName.equals("cota_nieve_prov")) {
            switch (period) {
                case "06-12":
                    meteo.setSnow(0, value + "/");
                    break;
                case "12-18":
                    meteo.setSnow(1, value + "/");
                    break;
                case "18-24":
                    meteo.setSnow(2, value);
                    break;
            }
        } else if (eventName.equals("estado_cielo")) {
            switch (period) {
                case "06-12":
                    meteo.setSky(0, value + "/");
                    break;
                case "12-18":
                    meteo.setSky(1, value + "/");
                    break;
                case "18-24":
                    meteo.setSky(2, value);
                    break;
            }
        } else if (eventName.equals("viento")) {
            switch (period) {
                case "06-12":
                    meteo.setWind(0, value + "/");
                    break;
                case "12-18":
                    meteo.setWind(1, value + "/");
                    break;
                case "18-24":
                    meteo.setWind(2, value);
                    break;
            }
        }
    }

    private static String replaceString(String text){

        if(text.charAt(0) == 'h')
            return text;

        byte beginIndex = -1,
                endIndex = -1;

        for(byte i = 0; i < text.length(); i++){
            if(text.charAt(i) == 'h')
                beginIndex = i;
            else if(text.charAt(i) == ']')
                endIndex = i;

        }

        return text.substring(beginIndex, endIndex);
    }
}

