package com.example.espino.xml.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by espino on 11/12/16.
 */

public class RSSNews  implements Parcelable{

    private String name;
    private String link;

    public RSSNews(String name, String link) {
        this.name = name;
        this.link = link;
    }

    protected RSSNews(Parcel in) {
        name = in.readString();
        link = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(link);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RSSNews> CREATOR = new Creator<RSSNews>() {
        @Override
        public RSSNews createFromParcel(Parcel in) {
            return new RSSNews(in);
        }

        @Override
        public RSSNews[] newArray(int size) {
            return new RSSNews[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
