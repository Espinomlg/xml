package com.example.espino.xml;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.espino.xml.models.Meteorology;
import com.example.espino.xml.models.RestClient;
import com.example.espino.xml.xmlMethods.XMLMethods;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;

import cz.msebera.android.httpclient.Header;

public class MeteorologyActivity extends AppCompatActivity {

    private TextView tempToday,
            uvToday,
            tempTomorrow,
            uvTomorrow;

    private LinearLayout rainProbToday,
            snowToday,
            windToday,
            skyToday,
            rainProbTomorrow,
            snowTomorrow,
            windTomorrow,
            skyTomorrow;

    private boolean wait;
    private Meteorology[] meteo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meterology);

        rainProbToday = (LinearLayout) findViewById(R.id.meteorology_linlay_raintoday);
        snowToday = (LinearLayout) findViewById(R.id.meteorology_linlay_snowtoday);
        windToday = (LinearLayout) findViewById(R.id.meteorology_linlay_windtoday);
        skyToday = (LinearLayout) findViewById(R.id.meteorology_linlay_skytoday);
        tempToday = (TextView) findViewById(R.id.meteorology_txv_tempvaluestoday);
        uvToday = (TextView) findViewById(R.id.meteorology_txv_UVtoday);

        rainProbTomorrow = (LinearLayout) findViewById(R.id.meteorology_linlay_raintomorrow);
        snowTomorrow = (LinearLayout) findViewById(R.id.meteorology_linlay_snowtomorrow);
        windTomorrow = (LinearLayout) findViewById(R.id.meteorology_linlay_windtomorrow);
        skyTomorrow = (LinearLayout) findViewById(R.id.meteorology_linlay_skytomorrow);
        tempTomorrow = (TextView) findViewById(R.id.meteorology_txv_tempvaluestomorrow);
        uvTomorrow = (TextView) findViewById(R.id.meteorology_txv_UVtomorrow);

        meteo = (Meteorology[]) getIntent().getSerializableExtra("meteo");
        loadData();


    }

    public void loadData(){

        for(byte i = 0; i <= 2; i++){
            TextView temporal = new TextView(getApplicationContext());
            temporal.setTextColor(getResources().getColor(R.color.black));
            temporal.setText(meteo[1].getWind(i));
            windTomorrow.addView(temporal);
        }
        for(byte i = 0; i <= 2; i++){
            TextView temporal = new TextView(getApplicationContext());
            temporal.setTextColor(getResources().getColor(R.color.black));
            temporal.setText(meteo[1].getSky(i));
            skyTomorrow.addView(temporal);
        }
        for(byte i = 0; i <= 2; i++){
            TextView temporal = new TextView(getApplicationContext());
            temporal.setTextColor(getResources().getColor(R.color.black));
            temporal.setText(meteo[0].getSnow(i));
            snowToday.addView(temporal);
        }
        for(byte i = 0; i <= 2; i++){
            TextView temporal = new TextView(getApplicationContext());
            temporal.setTextColor(getResources().getColor(R.color.black));
            temporal.setText(meteo[0].getWind(i));
            windToday.addView(temporal);
        }
        for(byte i = 0; i <= 2; i++){
            TextView temporal = new TextView(getApplicationContext());
            temporal.setTextColor(getResources().getColor(R.color.black));
            temporal.setText(meteo[0].getSky(i));
            skyToday.addView(temporal);
        }
        for(byte i = 0; i <= 2; i++){
            TextView temporal = new TextView(getApplicationContext());
            temporal.setTextColor(getResources().getColor(R.color.black));
            temporal.setText(meteo[1].getRainProb(i));
            rainProbTomorrow.addView(temporal);
        }
        for(byte i = 0; i <= 2; i++){
            TextView temporal = new TextView(getApplicationContext());
            temporal.setText(meteo[1].getSnow(i));
            snowTomorrow.addView(temporal);
        }
        tempToday.setText(meteo[0].getTemp());
        tempTomorrow.setText(meteo[1].getTemp());
        uvToday.setText(meteo[0].getUv());
        uvTomorrow.setText(meteo[1].getUv());
    }


}
