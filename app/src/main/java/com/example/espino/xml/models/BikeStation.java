package com.example.espino.xml.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by espino on 10/12/16.
 */

public class BikeStation implements Parcelable{

    private String address,
            state,
            bikesAvailable,
            anchorages,
            lastModification,
            link;

    public BikeStation(){

    }

    public BikeStation(Parcel in) {
        address = in.readString();
        state = in.readString();
        bikesAvailable = in.readString();
        anchorages = in.readString();
        lastModification = in.readString();
        link = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeString(state);
        dest.writeString(bikesAvailable);
        dest.writeString(anchorages);
        dest.writeString(lastModification);
        dest.writeString(link);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BikeStation> CREATOR = new Creator<BikeStation>() {
        @Override
        public BikeStation createFromParcel(Parcel in) {
            return new BikeStation(in);
        }

        @Override
        public BikeStation[] newArray(int size) {
            return new BikeStation[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state.equals("OPN") ? "abierta" : "cerrada";
    }

    public String getBikesAvailable() {
        return bikesAvailable;
    }

    public void setBikesAvailable(String bikesAvailable) {
        this.bikesAvailable = bikesAvailable;
    }

    public String getAnchorages() {
        return anchorages;
    }

    public void setAnchorages(String anchorages) {
        this.anchorages = anchorages;
    }

    public String getLastModification() {
        return lastModification;
    }

    public void setLastModification(String lastModification) {
        this.lastModification = lastModification.substring(11,16);
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
