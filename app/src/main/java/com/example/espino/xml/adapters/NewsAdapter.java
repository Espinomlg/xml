package com.example.espino.xml.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.espino.xml.R;
import com.example.espino.xml.models.New;

import java.util.ArrayList;

/**
 * Created by espino on 11/12/16.
 */

public class NewsAdapter extends ArrayAdapter<New> {



    public NewsAdapter(Context context, ArrayList<New> list) {
        super(context, R.layout.listitem_station, list);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View item = convertView;
        NewsAdapter.NewsHolder holder;

        if(item == null){
            item = LayoutInflater.from(getContext()).inflate(R.layout.listitem_station, parent, false);
            holder = new NewsAdapter.NewsHolder();

            holder.name = (TextView) item.findViewById(R.id.listitem_txv);

            item.setTag(holder);
        }
        else
            holder = (NewsAdapter.NewsHolder) item.getTag();

        holder.name.setText(getItem(position).getTitle());

        return item;
    }

    private static class NewsHolder{
        private TextView name;
    }
}

